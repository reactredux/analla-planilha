import { put } from 'redux-saga/effects'

import ActionCreators from '../actionCreators'

export const getFornecedores = ({ axios }) => function* () {
    try {
        const dados = yield axios.get('/fornecedores')
        yield put(ActionCreators.loadDataFornecedoresSuccess(dados.data.retorno))
    } catch (e) {
        yield put(ActionCreators.loadDataFornecedoresFailure('Ocorreu um erro'))
    }
}