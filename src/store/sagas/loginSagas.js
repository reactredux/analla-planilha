import { put } from 'redux-saga/effects'
import ActionCreators from '../actionCreators'

import { setLocalstorage, delLocalstorage } from '../../components/util/storage'

export const logar = ({ axios }) => function* (action) {

    delLocalstorage('usuario_logado')
    delLocalstorage('token')

    const formData = new URLSearchParams();
    formData.append('login', action.login);
    formData.append('senha', action.senha);

    try {
        const dados = yield axios.post('/admin/logar', formData)

        setLocalstorage('usuario_logado', dados.data.retorno)
        setLocalstorage('token', dados.data.token)

        yield put(ActionCreators.loginSuccess(dados.data))
    } catch (e) {
        yield put(ActionCreators.loginFailure('Os dados informados são inválidos'))
    }
}

export const logout = ({ axios }) => function* () {
    delLocalstorage('usuario_logado')
    delLocalstorage('token')

    yield put(ActionCreators.logoutSuccess())
}