import { put } from 'redux-saga/effects'

import ActionCreators from '../actionCreators'

export const getClientes = ({ axios }) => function* () {
    try {
        const dados = yield axios.get('/clientes')
        yield put(ActionCreators.loadDataClientesSuccess(dados.data.retorno))
    } catch (e) {
        yield put(ActionCreators.loadDataClientesFailure('Ocorreu um erro'))
    }
}