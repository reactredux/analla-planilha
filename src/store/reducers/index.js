import { combineReducers } from 'redux'

import fornecedoresReducer from './fornecedoresReducer'
import clientesReducer from './clientesReducer'
import loginReducer from './loginReducer'

const rootReducer = combineReducers({
    retFornecedoresReducer: fornecedoresReducer,
    retClientesReducer: clientesReducer,
    retLoginReducer: loginReducer,
})

export default rootReducer