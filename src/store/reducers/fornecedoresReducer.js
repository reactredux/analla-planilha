import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'

export const INITIAL_STATE = {
    data: [],
    isLoading: false,
    error: false,
    msgError: ''
}

export const loadDataFornecedoresRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true,
        error: false,
        msgError: ''
    }
}

export const loadDataFornecedoresSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: action.data,
        isLoading: false,
        error: false,
        msgError: ''
    }
}

export const loadDataFornecedoresFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        error: true,
        msgError: action.msg
    }
}

export const HANDLERS = {
    [Types.LOAD_DATA_FORNECEDORES_REQUEST]: loadDataFornecedoresRequest,
    [Types.LOAD_DATA_FORNECEDORES_SUCCESS]: loadDataFornecedoresSuccess,
    [Types.LOAD_DATA_FORNECEDORES_FAILURE]: loadDataFornecedoresFailure
}

export default createReducer(INITIAL_STATE, HANDLERS)