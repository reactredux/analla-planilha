import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'
import { getLocalstorage } from '../../components/util/storage'

export const INITIAL_STATE = {
    data: [],
    isLoggedIn: getLocalstorage('token') || false,
    isLoading: false,
    error: false,
    msgError: ''
}

export const loginRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true,
        error: false,
        msgError: ''
    }
}

export const loginSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: action.data,
        isLoggedIn: true,
        isLoading: false,
        error: false,
        msgError: ''
    }
}

export const loginFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        error: true,
        msgError: action.msg
    }
}

export const logoutRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: [],
        isLoggedIn: false,
        isLoading: false,
        error: false,
        msgError: ''
    }
}

export const logoutSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: [],
        isLoggedIn: false,
        isLoading: false,
        error: false,
        msgError: ''
    }
}

export const HANDLERS = {
    [Types.LOGIN_REQUEST]: loginRequest,
    [Types.LOGIN_SUCCESS]: loginSuccess,
    [Types.LOGIN_FAILURE]: loginFailure,

    [Types.LOGOUT_REQUEST]: logoutRequest,
    [Types.LOGOUT_SUCCESS]: logoutSuccess
}

export default createReducer(INITIAL_STATE, HANDLERS)