import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'

export const INITIAL_STATE = {
    data: [],
    isLoading: false,
    error: false,
    msgError: ''
}

export const loadDataClientesRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true,
        error: false,
        msgError: ''
    }
}

export const loadDataClientesSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: action.data,
        isLoading: false,
        error: false,
        msgError: ''
    }
}

export const loadDataClientesFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        error: true,
        msgError: action.msg
    }
}

export const HANDLERS = {
    [Types.LOAD_DATA_CLIENTES_REQUEST]: loadDataClientesRequest,
    [Types.LOAD_DATA_CLIENTES_SUCCESS]: loadDataClientesSuccess,
    [Types.LOAD_DATA_CLIENTES_FAILURE]: loadDataClientesFailure
}

export default createReducer(INITIAL_STATE, HANDLERS)