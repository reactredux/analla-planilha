import { createActions } from 'reduxsauce'

export const { Types, Creators } = createActions({
    loginRequest: ['login', 'senha'],
    loginSuccess: ['data'],
    loginFailure: ['msg'],

    logoutRequest: [''],
    logoutSuccess: [''],

    loadDataClientesRequest: [''],
    loadDataClientesSuccess: ['data'],
    loadDataClientesFailure: ['msg'],

    loadDataFornecedoresRequest: [''],
    loadDataFornecedoresSuccess: ['data'],
    loadDataFornecedoresFailure: ['msg'],

})

export default Creators