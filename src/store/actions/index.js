import { takeLatest, all } from 'redux-saga/effects'
import { Types } from '../actionCreators'

import axios from 'axios'
import { getLocalstorage, delLocalstorage } from '../../components/util/storage'

import { logar, logout } from '../sagas/loginSagas'
import { getClientes } from '../sagas/clientesSagas'
import { getFornecedores } from '../sagas/fornecedoresSagas';

// axios.defaults.baseURL = 'http://www.alvoideal.com.br/uploads/restAnalla'; // (package.json => proxy)

axios.interceptors.request.use(function (config) {
    if (config.url.indexOf('/admin/logar') === -1) {
        config.headers.common['token'] = getLocalstorage('token');
    }

    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(function (config) {
    return config;
}, function (error) {

    if (error.response.status === 401 || error.response.status === 403) {
        delLocalstorage('usuario_logado')
        delLocalstorage('token')
        window.location.href = '/login'
    }

    return Promise.reject(error.response);
});

export default function* rootSaga() {
    yield all([
        takeLatest(Types.LOGIN_REQUEST, logar({ axios })),
        takeLatest(Types.LOGOUT_REQUEST, logout({ axios })),
        takeLatest(Types.LOAD_DATA_CLIENTES_REQUEST, getClientes({ axios })),
        takeLatest(Types.LOAD_DATA_FORNECEDORES_REQUEST, getFornecedores({ axios })),
    ])
}