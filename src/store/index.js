import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
// import logger from 'redux-logger'

import rootReducer from './reducers'
import indexSaga from './actions'

const sagaMiddlware = createSagaMiddleware()

const store = createStore(
    rootReducer,
    applyMiddleware(
        sagaMiddlware
        /*, logger*/
    )
)

sagaMiddlware.run(indexSaga)

export default store