import { fundo } from "./assets";

// https://material.io/tools/icons/?style=baseline

// https://material-ui.com/layout/grid/

const styles = () => ({
  grow: {
    flexGrow: 1,
  },
  painelFundo: {
    background: "#EEEEEE",
    width: '100vw',
    height: 'calc(100vh - 65px)',
    paddingTop: 65,
    bottom: 0,
    position: 'absolute',
  },
  menuAppBar: {
    // height: 65,
  },
  loginFundo: {
    backgroundImage: 'url(' + fundo + ')',
    backgroundSize: 'cover',
    overflow: 'hidden',
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
  },
  loginPainel: {
    background: '#FFFFFF',
    borderRadius: 10,
    padding: 50,
    textAlign: 'center',
  },
  loginMsgErro: {
    color: '#ff3366',
  },
  menuFlutuante: {
    width: 300,
  },
  menuIconConfig: {
    color: '#f50057',
  },
  menuCard: {
    padding: 1,
    margin: 0,
  },
  menuAvatar: {
    backgroundColor: '#bdbdbd'
  },
  menuAvatarIcon: {
    color: '#ffffff'
  },

  card: {
    margin: 25,
    marginTop: 50,
    borderRadius: 10,
    boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
  },

  cardContent: {
    paddingTop: 75,
  },

  cardHeader: {
    position: 'fixed',
    width: 'calc(100vw - 110px)',
    left: 40,
    padding: 15,
    marginTop: -20,
    borderRadius: 3,
    boxShadow: "0 12px 20px -10px rgba(255, 152, 0, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(255, 152, 0, 0.2)",
    background: '#ffa726',
  },

  checkedIcon: {
    width: "20px",
    height: "20px",
    border: "1px solid rgba(0, 0, 0, .54)",
    borderRadius: "3px"
  },
  uncheckedIcon: {
    width: "0px",
    height: "0px",
    padding: "10px",
    border: "1px solid rgba(0, 0, 0, .54)",
    borderRadius: "3px"
  },
  table: {
    marginBottom: "0",
    overflow: "visible"
  },
  tableRow: {
    position: "relative",
    borderBottom: "1px solid #dddddd"
  },
  tableHeader: {
    color: '#ff9800',
  },
  tableActions: {
    display: "flex",
    border: "none",
    padding: "12px 8px !important",
    verticalAlign: "middle"
  },
  tableHeadCell: {
    color: "inherit",
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: "300",
    lineHeight: "1.5em",
    fontSize: "1em"
  },
  tableCell: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: "300",
    lineHeight: "1.5em",
    padding: "8px",
    verticalAlign: "middle",
    border: "none",
    fontSize: "14px"
  },
  tableActionButton: {
    width: "27px",
    height: "27px",
    padding: "0"
  },
  tableActionButtonIcon: {
    width: "17px",
    height: "17px"
  },
  tableButton: {
    width: "50px",
    height: "50px",
    padding: "0"
  },
  tableButtonIcon: {
    width: "25px",
    height: "25px"
  },
  edit: {
    backgroundColor: "transparent",
    color: '#9c27b0',
    boxShadow: "none"
  },
  close: {
    backgroundColor: "transparent",
    color: '#f44336',
    boxShadow: "none"
  },
  marginBtn: {
    margin: 10,
  },
});

export default styles