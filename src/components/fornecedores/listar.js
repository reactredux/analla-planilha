import React, { Component } from 'react'
import { connect } from 'react-redux'
import ActionCreators from '../../store/actionCreators'

export class Listar extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    renderItem = (item, i) => {
        return <div key={i}>{item.nome}</div>
    }

    render() {
        const { isLoading, error, msgError, data } = this.props

        return (
            <div>
                {error &&
                    <div>{msgError}</div>
                }

                {!error &&
                    <div>
                        {isLoading && <div>Aguarde...</div>}
                        {!isLoading && <div>{data.map((item, i) => this.renderItem(item, i))}</div>}
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.retFornecedoresReducer.isLoading,
        data: state.retFornecedoresReducer.data,
        error: state.retFornecedoresReducer.error,
        msgError: state.retFornecedoresReducer.msgError,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(ActionCreators.loadDataFornecedoresRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Listar)