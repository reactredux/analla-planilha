import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import ActionCreators from '../store/actionCreators'

import { Grid } from "@material-ui/core";
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import { getLocalstorage } from './util/storage';
import { logo } from '../assets/assets';

export class Login extends Component {
    state = {
        login: 'admin',
        senha: '158210'
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    validar = (e) => {
        e.preventDefault();
        this.props.validar(this.state.login, this.state.senha);
    }

    render() {
        const { classes } = this.props

        if (getLocalstorage('token') || this.props.isLoggedIn) {
            return <Redirect to='/admin' />
        }

        return (
            <Grid container className={classes.loginFundo}>
                <Grid item xs={12}>

                    <Grid container justify="center">
                        <Grid item className={classes.loginPainel}>

                            <img src={logo} width="250" alt="" />
                            <br />

                            <TextField hintText="Login" floatingLabelText="Login" name="login" onChange={this.handleChange} defaultValue={this.state.login} />
                            <br />
                            <TextField hintText="Senha" floatingLabelText="Senha" name="senha" onChange={this.handleChange} defaultValue={this.state.senha} type="password" />
                            <br />
                            <RaisedButton label={this.props.isLoading? 'Aguarde...' : 'Entrar'} primary={true} onClick={this.validar} disabled={this.props.isLoading} />

                            {this.props.error &&
                                <p className={classes.loginMsgErro}>{this.props.msgError}</p>
                            }

                        </Grid>
                    </Grid>

                </Grid>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.retLoginReducer.isLoggedIn,
        isLoading: state.retLoginReducer.isLoading,
        data: state.retLoginReducer.data,
        error: state.retLoginReducer.error,
        msgError: state.retLoginReducer.msgError,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        validar: (login, senha) => dispatch(ActionCreators.loginRequest(login, senha))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)