import React, { Component } from 'react'
import { connect } from 'react-redux'
import ActionCreators from '../../store/actionCreators'
import { formatarValor, formatarData } from '../util/funcoes'

import { Card, CardHeader, CardContent, Table, TableBody, TableRow, TableCell, Checkbox, Tooltip } from "@material-ui/core";

// import Edit from "@material-ui/icons/Edit";
// import Close from "@material-ui/icons/Close";
// import Check from "@material-ui/icons/Check";
// import Avatar from '@material-ui/core/Avatar';
import TableHead from "@material-ui/core/TableHead";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import FilterListIcon from '@material-ui/icons/FilterList';
import TableSortLabel from '@material-ui/core/TableSortLabel';

export class Listar extends Component {

    state = {
        data: [],
        blqEditar: true,
        blqExcluir: true,
        checked: [],
        orderType: 'numeric',
        orderField: 'id',
        order: 'desc',
    };

    componentDidMount() {
        // this.props.loadData()

        this.setState({
            data: [
                { id: 1, titulo: 'Sign contract for "What are conference organizers afraid of?"', valor: 1.2, data: '2018-02-24' },
                { id: 12, titulo: "Lines From Great Russian Literature? Or E-mails From My Boss?", valor: 2.3, data: '2017-10-05' },
                { id: 13, titulo: "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit", valor: 1.1, data: '2018-01-21' },
                { id: 5, titulo: "Create 4 Invisible User Experiences you Never Knew About", valor: 0.2, data: '2019-11-11' }
            ]
        }, () => {
            this.refazOrdenacao();
        })
    }

    toogleCheck = (id) => {
        if (this.state.checked.indexOf(id) === -1) {
            return false;
        }

        return true;
    }

    ordenacao = (col, type, order, field) => {
        if (type === "numeric") {
            if (order === "asc") {
                col.sort((a, b) => {
                    return a[field] - b[field];
                })
            }
            if (order === "desc") {
                col.sort((a, b) => {
                    return b[field] - a[field];
                })
            }
        }

        if (type === "string") {
            if (order === "asc") {
                col.sort((a, b) => {
                    return a[field].toLowerCase().localeCompare(b[field].toLowerCase());
                })
            }
            if (order === "desc") {
                col.sort((a, b) => {
                    return b[field].toLowerCase().localeCompare(a[field].toLowerCase());
                })
            }
        }

        if (type === "date") {
            if (order === "asc") {
                col.sort((a, b) => {
                    return new Date(a[field]) - new Date(b[field])
                })
            }
            if (order === "desc") {
                col.sort((a, b) => {
                    return new Date(b[field]) - new Date(a[field])
                })
            }
        }

        return col;
    }

    refazOrdenacao = () => {
        var col = [...this.state.data];
        col = this.ordenacao(col, this.state.orderType, this.state.order, this.state.orderField)
        this.setState({ data: col })
    }

    handleToggle = (value) => {
        const { checked } = this.state;
        const currentIndex = checked.indexOf(value.id);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value.id);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked
        }, () => {

            this.setState({ blqEditar: true, blqExcluir: true })

            if (this.state.checked.length > 0) {
                if (this.state.checked.length === 1) {
                    this.setState({ blqEditar: false })
                }
                if (this.state.checked.length > 0) {
                    this.setState({ blqExcluir: false })
                }
            }

        });
    }

    btnInserir = () => {
        console.log('1')
    }

    btnEditar = () => {
        console.log('2', this.state.checked[0])
    }

    btnExcluir = () => {
        console.log('3', this.state.checked)
    }

    renderTopo = () => {
        const { classes } = this.props

        return (
            <Toolbar className={classes.root}>
                <Fab color="primary" aria-label="Add" onClick={() => this.btnInserir()} className={classes.fab + " " + classes.marginBtn} size="small" >
                    <AddIcon />
                </Fab>
                <Fab disabled={this.state.blqEditar} onClick={() => this.btnEditar()} color="secondary" aria-label="Edit" className={classes.fab + " " + classes.marginBtn} size="small" >
                    <Icon>edit_icon</Icon>
                </Fab>
                <Fab disabled={this.state.blqExcluir} onClick={() => this.btnExcluir()} aria-label="Delete" className={classes.fab + " " + classes.marginBtn} size="small" >
                    <DeleteIcon />
                </Fab>

            </Toolbar>
        );
    }

    renderItem = (item) => {
        const { classes } = this.props

        return (
            <TableRow hover onClick={() => this.handleToggle(item)} key={item.id} className={classes.tableRow}>
                <TableCell padding="checkbox">
                    <Checkbox checked={this.toogleCheck(item.id)} />
                </TableCell>

                <TableCell className={classes.tableCell}>
                    {item.id}
                </TableCell>

                <TableCell className={classes.tableCell}>
                    {item.titulo}
                </TableCell>

                <TableCell className={classes.tableCell}>
                    {formatarValor(item.valor)}
                </TableCell>

                <TableCell className={classes.tableCell}>
                    {formatarData(item.data)}
                </TableCell>
            </TableRow>
        )
    }

    handleRequestSort = (row) => {
        const isDesc = this.state.orderField === row.id && this.state.order === 'desc';
        this.setState({ order: (isDesc ? 'asc' : 'desc') })
        this.setState({ orderField: row.id })
        this.setState({ orderType: row.type })

        setTimeout(() => {
            this.refazOrdenacao();
        }, 100)
    }

    render() {
        const { isLoading, error, msgError, classes } = this.props
        const { data } = this.state

        const headerRow = [
            { id: 'id', type: 'numeric', label: 'ID' },
            { id: 'titulo', type: 'string', label: 'Titulo' },
            { id: 'valor', type: 'numeric', label: 'Valor' },
            { id: 'data', type: 'date', label: 'Data' }
        ];

        return (
            <div>
                <Card className={classes.card}>
                    <CardHeader className={classes.cardHeader} title="Clientes" subheader="Cadastro de Clientes" />

                    <CardContent className={classes.cardContent}>
                        {error &&
                            <div>{msgError}</div>
                        }

                        {!error &&
                            <div>
                                {isLoading && <div>Aguarde...</div>}
                                {!isLoading &&
                                    <div>
                                        {this.renderTopo()}

                                        <Table className={classes.table}>

                                            <TableHead>
                                                <TableRow>

                                                    <TableCell padding="checkbox">
                                                    </TableCell>

                                                    {
                                                        headerRow.map((row) => {
                                                            return (
                                                                <TableCell key={row.id} align='center' sortDirection={this.state.orderField === row.id ? this.state.order : false}>
                                                                    <Tooltip title="Sort" placement='bottom-start' enterDelay={300}>
                                                                        <TableSortLabel active={this.state.orderField === row.id} direction={this.state.order} onClick={() => this.handleRequestSort(row)}>
                                                                            {row.label}
                                                                        </TableSortLabel>
                                                                    </Tooltip>
                                                                </TableCell>
                                                            )
                                                        })
                                                    }

                                                </TableRow>
                                            </TableHead>

                                            <TableBody>
                                                {data.map((item) => this.renderItem(item))}
                                            </TableBody>
                                        </Table>
                                    </div>}
                            </div>
                        }
                    </CardContent>
                </Card>


            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.retClientesReducer.isLoading,
        data: state.retClientesReducer.data,
        error: state.retClientesReducer.error,
        msgError: state.retClientesReducer.msgError,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadData: () => dispatch(ActionCreators.loadDataClientesRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Listar)