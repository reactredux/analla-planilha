import React, { Component } from 'react'

import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

export class Editar extends Component {

    state = {
        bio: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    continue = (e) => {
        e.preventDefault();
        // this.props.salvar();
    }


    render() {
        return (
            <div>
                <TextField hintText="Enter your bio" floatingLabelText="Bio" name="bio" onChange={this.handleChange} defaultValue={values.bio} />
                <br />
                <RaisedButton label='Continue' primary={true} onClick={this.continue} />
            </div>
        )
    }
}

export default Editar
