import React, { Component } from 'react'
import { connect } from 'react-redux'
import ActionCreators from '../store/actionCreators'
import { Redirect } from 'react-router-dom'
import { Route } from 'react-router-dom'

import { Grid, AppBar, Paper, ListItem, List, ListItemText, ListItemSecondaryAction, Toolbar, IconButton } from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import Switch from '@material-ui/core/Switch';

import Menu from './util/menu'
import CardMenu from './util/cardMenu'
import { getLocalstorage, setLocalstorage } from './util/storage'

import Drawer from '@material-ui/core/Drawer';
import { logo } from '../assets/assets';

import ListarProdutos from './produtos/listar'
import ListarClientes from './clientes/listar'
import ListarFornecedores from './fornecedores/listar'
import Principal from './principal';
import Perfil from './perfil';
import Pedidos from './pedidos';

const Departamentos = () => <div>depart</div>

export class Admin extends Component {

    state = {
        usuario: {
            nome: 'Anderson H. Botega',
            email: 'anderson@servidor.com.br',
        },
        emissaoNFe: true,
        cobrarFrete: false,
        menuVisivel: true,
        expandirMenu: true
    }

    componentWillMount() {
        this.setState({
            menuVisivel: getLocalstorage('expandirMenu'),
            expandirMenu: getLocalstorage('expandirMenu')
        })
    }

    logout = (e) => {
        e.preventDefault();
        this.props.logout();
    }

    toggleDrawer = (open) => {
        this.setState({ menuVisivel: open });
    }

    salvaMenu = (e) => {
        this.setState({
            expandirMenu: e.target.checked
        });

        setLocalstorage('expandirMenu', e.target.checked)
    }

    handleToggle = (e) => {
        this.setState({
            [e.target.name]: e.target.checked
        })
    }

    render() {
        const { classes } = this.props

        if (!getLocalstorage('token') || !this.props.isLoggedIn) {
            return <Redirect to='/login' />
        }

        return (
            <div>
                <AppBar className={classes.menuAppBar} >
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={() => this.toggleDrawer(true)}>
                            <MenuIcon />
                        </IconButton>

                        <img src={logo} height="50" alt="" />
                    </Toolbar>
                </AppBar>

                <Grid container>
                    <Grid item xs={12}>
                        <Drawer open={this.state.menuVisivel} onClose={() => this.toggleDrawer(false)}>

                            <CardMenu classes={classes} usuario={this.state.usuario} onOcultarMenu={() => this.toggleDrawer(false)} />

                            <div tabIndex={0} role="button" onClick={() => this.toggleDrawer(false)} onKeyDown={() => this.toggleDrawer(false)}>
                                <Menu classes={classes} logout={this.logout} />
                            </div>

                            <List>
                                <ListItem>
                                    <ListItemText primary="Emissão de NFe" />
                                    <ListItemSecondaryAction>
                                        <Switch onChange={(e) => this.handleToggle(e)} name='emissaoNFe' checked={this.state.emissaoNFe} />
                                    </ListItemSecondaryAction>
                                </ListItem>

                                <ListItem>
                                    <ListItemText primary="Cobrar Frete" />
                                    <ListItemSecondaryAction>
                                        <Switch onChange={(e) => this.handleToggle(e)} name='cobrarFrete' checked={this.state.cobrarFrete} />
                                    </ListItemSecondaryAction>
                                </ListItem>

                                <ListItem>
                                    <ListItemText primary="Sempre expandir o menu" />
                                    <ListItemSecondaryAction>
                                        <Switch onChange={(e) => this.salvaMenu(e)} name='expandirMenu' checked={this.state.expandirMenu} />
                                    </ListItemSecondaryAction>
                                </ListItem>
                            </List>
                        </Drawer>

                        <Paper className={classes.painelFundo}>
                            <Route path='/admin/' component={Principal} exact />
                            <Route path='/admin/perfil' component={Perfil} />
                            <Route path='/admin/clientes' render={(props) => <ListarClientes classes={classes} {...props} />} />
                            <Route path='/admin/fornecedores' component={ListarFornecedores} />
                            <Route path='/admin/produtos' component={ListarProdutos} />
                            <Route path='/admin/pedidos' component={Pedidos} />
                            <Route path='/admin/departamentos' component={Departamentos} />
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.retLoginReducer.isLoggedIn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(ActionCreators.logoutRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)