export const setLocalstorage = (chave, valor) => {
    localStorage.setItem(chave, JSON.stringify(valor));
}

export const getLocalstorage = (chave) => {
    return JSON.parse(localStorage.getItem(chave))
}

export const delLocalstorage = (chave) => {
    localStorage.removeItem(chave);
}