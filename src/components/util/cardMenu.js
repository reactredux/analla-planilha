import React from 'react'

import { Grid, ListItem, List, GridListTile, GridList, GridListTileBar, IconButton } from "@material-ui/core";
import { Link } from 'react-router-dom'

import VpnKeyIcon from '@material-ui/icons/Tune';

import { anderson } from '../../assets/assets';

const CardMenu = ({ classes, usuario, onOcultarMenu }) => {
    return (
        <div className={classes.menuFlutuante}>
            <Grid container>
                <Grid item xs={12}>
                    <List classes={{ root: classes.menuCard }}>
                        <ListItem classes={{ root: classes.menuCard }}>
                            <GridList cols={1}>
                                <GridListTile>
                                    <img src={anderson} width="300" height="220" alt="" />

                                    <GridListTileBar title={usuario.nome} subtitle={usuario.email}
                                        actionIcon={
                                            <ListItem component={Link} to='/admin/perfil' button>
                                                <IconButton className={classes.menuIconConfig} onClick={onOcultarMenu}>
                                                    <VpnKeyIcon />
                                                </IconButton>
                                            </ListItem>
                                        } />
                                </GridListTile>
                            </GridList>
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
        </div>
    )
}

export default CardMenu