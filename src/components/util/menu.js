import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Grid, Avatar, List, ListItem, ListItemText, Divider } from "@material-ui/core";

import HomeIcon from "@material-ui/icons/Home";
import ClientesIcon from "@material-ui/icons/AccountCircle";
import FornecedoresIcon from "@material-ui/icons/PermContactCalendar";
import ProdutosIcon from "@material-ui/icons/Work";
import PedidosIcon from "@material-ui/icons/Assignment";
import LogoutIcon from '@material-ui/icons/CancelPresentation';
import DepartamentosIcon from '@material-ui/icons/AssignmentTurnedIn';

export class Menu extends Component {

    render() {
        const list = [
            { id: 0, titulo: "Principal", descricao: "", icone: <HomeIcon />, url: "/admin/" },
            { id: 1, titulo: "Clientes", descricao: "Cadastro de Clientes", icone: <ClientesIcon />, url: "/admin/clientes" },
            { id: 5, titulo: "Fornecedores", descricao: "Cadastro de Fornecedores", icone: <FornecedoresIcon />, url: "/admin/fornecedores" },
            { id: 4, titulo: "Departamentos", descricao: "Categorias e subcategorias", icone: <DepartamentosIcon />, url: "/admin/departamentos" },
            { id: 2, titulo: "Produtos", descricao: "Cadastro de Produtos", icone: <ProdutosIcon />, url: "/admin/produtos" },
            { id: 3, titulo: "Pedidos", descricao: "Gerenciamento de Pedidos", icone: <PedidosIcon />, url: "/admin/pedidos" },
        ];

        const { classes } = this.props

        return (
            <Grid container>
                <Grid item xs={12}>
                    <List>
                        <Divider />

                        {list.map(item => (
                            <ListItem component={Link} to={item.url} button key={item.id}>
                                <Avatar>{item.icone}</Avatar>
                                <ListItemText primary={item.titulo} secondary={item.descricao} />
                            </ListItem>
                        ))}

                        <Divider />

                        <ListItem button onClick={(e) => this.props.logout(e)}>
                            <Avatar className={classes.menuAvatar}><LogoutIcon className={classes.menuAvatarIcon} /></Avatar>
                            <ListItemText primary="Sair do sistema" />
                        </ListItem>

                        <Divider />
                    </List>
                </Grid>
            </Grid>
        )
    }
}

export default Menu