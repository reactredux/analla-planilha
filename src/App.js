import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles";
import './App.css';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import { Provider } from 'react-redux'
import store from './store/index'

import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'

import Login from './components/login';
import Admin from './components/admin';
import styles from './assets/styles';

const pg404 = () => <div>Pagina não encontrada!</div>

class App extends Component {
  render() {
    const { classes } = this.props

    return (
      <Provider store={store}>
        <Router>
          <MuiThemeProvider>
            <React.Fragment>

              <Switch>
                <Route exact path='/' render={(props) => <Login classes={classes} {...props} />} />
                <Route path='/login' render={(props) => <Login classes={classes} {...props} />} />
                <Route path='/admin' render={(props) => <Admin classes={classes} {...props} />} />
                <Route component={pg404} />
              </Switch>

            </React.Fragment>
          </MuiThemeProvider>
        </Router>
      </Provider>
    );
  }
}

export default withStyles(styles)(App);